package com.doodle.rav.doodletranslate.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by rav on 14.05.2018.
 */

@Entity(tableName = "cached_translation")
public class CachedTranslation {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "form_to")
    private String codeFromToLang;

    @ColumnInfo(name = "text_in")
    private String textInput;

    @ColumnInfo(name = "text_out")
    private String textOutput;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodeFromToLang() {
        return codeFromToLang;
    }

    public void setCodeFromToLang(String codeFromToLang) {
        this.codeFromToLang = codeFromToLang;
    }

    public String getTextInput() {
        return textInput;
    }

    public void setTextInput(String textInput) {
        this.textInput = textInput;
    }

    public String getTextOutput() {
        return textOutput;
    }

    public void setTextOutput(String textOutput) {
        this.textOutput = textOutput;
    }
}
