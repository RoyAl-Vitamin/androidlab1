package com.doodle.rav.doodletranslate.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.doodle.rav.doodletranslate.db.entity.CachedTranslation;

import java.util.List;

/**
 * Created by rav on 14.05.2018.
 */

@Dao
public interface CachedTranslationDAO {

    @Query("SELECT * FROM cached_translation")
    List<CachedTranslation> getAll();

    @Query("SELECT * FROM cached_translation ct WHERE ct.text_in = :text AND ct.form_to = :fromTo LIMIT 1")
    CachedTranslation findByTextAndLang(String text, String fromTo);

    @Insert
    void insertAll(CachedTranslation... cachedTranslation);

    @Delete
    void delete(CachedTranslation cachedTranslation);

    @Insert
    void insert(CachedTranslation cachedTranslation);
}
