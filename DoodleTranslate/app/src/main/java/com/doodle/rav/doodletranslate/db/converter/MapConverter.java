package com.doodle.rav.doodletranslate.db.converter;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by rav on 21.05.2018.
 */

public class MapConverter {

    @TypeConverter
    public String fromMap(Map<String, String> map) {
        return new Gson().toJson(map);
    }

    @TypeConverter
    public Map<String, String> toMap(String data) {
        return (Map<String, String>) new Gson().fromJson(data, Map.class);
    }
}
