package com.doodle.rav.doodletranslate.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.doodle.rav.doodletranslate.db.dao.CachedLangMapDAO;
import com.doodle.rav.doodletranslate.db.dao.CachedTranslationDAO;
import com.doodle.rav.doodletranslate.db.entity.CachedLangPair;
import com.doodle.rav.doodletranslate.db.entity.CachedTranslation;

/**
 * Created by rav on 14.05.2018.
 */

@Database(entities = {CachedTranslation.class, CachedLangPair.class}, version = 7)
public abstract class AppDatabase extends RoomDatabase {

    public abstract CachedTranslationDAO cachedTranslationDAO();

    public abstract CachedLangMapDAO cachedLangMapDAO();

}
