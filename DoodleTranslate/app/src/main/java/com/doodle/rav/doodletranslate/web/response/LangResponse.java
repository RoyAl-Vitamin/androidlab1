package com.doodle.rav.doodletranslate.web.response;

import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by rav on 14.05.2018.
 */

public class LangResponse {

    @SerializedName("dirs")
    @Expose
    @Deprecated
    private List<String> dirs = null;

    @SerializedName("langs")
    @Expose
    private Map<String, String> langs = null;

    @SerializedName("code")
    @Expose
    private Integer code;

    @SerializedName("message")
    @Expose
    private String message;

    public String getMessageByCode() {
        return map.get(code);
    }

    @Deprecated
    public List<String> getDirs() {
        return dirs;
    }

    public void setDirs(List<String> dirs) {
        this.dirs = dirs;
    }

    public Map<String, String> getLangs() {
        return langs;
    }

    public void setLangs(Map<String, String> langs) {
        this.langs = langs;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    static Map<String, String> map = new HashMap<>();
    static {
        map.put("401","Неправильный API-ключ");
        map.put("402","API-ключ заблокирован");
    }
}
