package com.doodle.rav.doodletranslate.fragment;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.doodle.rav.doodletranslate.R;
import com.doodle.rav.doodletranslate.utils.SharedViewModel;

import org.androidannotations.annotations.AfterTextChange;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by rav on 08.05.2018.
 */

@EFragment(R.layout.fragment_settings)
public class SettingsTranslate extends Fragment {

    private static final String TAG = "SettingsTranslate";

    private SharedViewModel model;

    @ViewById(R.id.etKey)
    EditText etKey;

    @ViewById(R.id.tvCopyRight)
    TextView evCR;

    @AfterTextChange(R.id.etKey)
    void afterTextChangedOnHelloTextView(Editable text, TextView textView) {
        model.setApiKey(textView.getText().toString());
    }

    @AfterViews
    void init() {
        evCR.setMovementMethod(LinkMovementMethod.getInstance());
        etKey.setText(model.getApiKey().getValue());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);
    }
}
