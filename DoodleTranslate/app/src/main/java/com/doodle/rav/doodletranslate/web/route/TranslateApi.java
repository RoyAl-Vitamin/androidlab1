package com.doodle.rav.doodletranslate.web.route;

import com.doodle.rav.doodletranslate.web.response.TranslateResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by rav on 14.05.2018.
 */

/**
 * Emample from: https://tech.yandex.ru/translate/doc/dg/reference/translate-docpage/
 * https://translate.yandex.net/api/v1.5/tr.json/translate
 * ? [key=<API-ключ>]
 * & [text=<переводимый текст>]
 * & [lang=<направление перевода>]
 * & [format=<формат текста>]
 * & [options=<опции перевода>]
 */
public interface TranslateApi {
    @GET("/api/v1.5/tr.json/translate")
    Call<TranslateResponse> getData(
            @Query("key") String key, // API-Key
            @Query("text") String text, // text
            @Query("lang") String lang, // FROM-TO Lang, ex: "ru-en"
            @Query("format") String format, // "plain" or "html"
            @Query("options") String options); // "0" or "1" for autodetect lang text
}
