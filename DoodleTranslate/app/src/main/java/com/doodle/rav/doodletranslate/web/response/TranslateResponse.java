package com.doodle.rav.doodletranslate.web.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by rav on 14.05.2018.
 */

public class TranslateResponse {

    @SerializedName("code")
    @Expose
    private int code;

    @SerializedName("lang")
    @Expose
    private String lang;

    @SerializedName("text")
    @Expose
    private List<String> text = null;

    public String getMessageByCode() {
        return map.get(code);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public List<String> getText() {
        return text;
    }

    public void setText(List<String> text) {
        this.text = text;
    }

    static Map<String, String> map = new HashMap<>();
    static {
        map.put("200","Операция выполнена успешно");
        map.put("401","Неправильный API-ключ");
        map.put("402","API-ключ заблокирован");
        map.put("404","Превышено суточное ограничение на объем переведенного текста");
        map.put("413","Превышен максимально допустимый размер текста");
        map.put("422","Текст не может быть переведен");
        map.put("501","Заданное направление перевода не поддерживается");
    }
}
