package com.doodle.rav.doodletranslate.web.route;

import com.doodle.rav.doodletranslate.web.response.LangResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by rav on 14.05.2018.
 */


/**
 * Emample from: https://tech.yandex.ru/translate/doc/dg/reference/getLangs-docpage/
 * https://translate.yandex.net/api/v1.5/tr.json/getLangs
 * ? [key=<API-ключ>]
 * & [ui=<код языка>]
 * & [callback=<имя callback-функции>]
 */

public interface AvailableLanguageApi {
    @GET("/api/v1.5/tr.json/getLangs")
    Call<LangResponse> getData(@Query("key") String key, @Query("ui") String codeLang);
}
