package com.doodle.rav.doodletranslate.application;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.doodle.rav.doodletranslate.db.AppDatabase;
import com.doodle.rav.doodletranslate.web.route.AvailableLanguageApi;
import com.doodle.rav.doodletranslate.web.route.TranslateApi;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by rav on 14.05.2018.
 */

public class App extends Application {

    private static final String BASE_URL = "https://translate.yandex.net";

    private static AppDatabase DB;

    private Retrofit retrofit;

    private static AvailableLanguageApi availableLanguageApi;

    private static TranslateApi translateApi;

    @Override
    public void onCreate() {
        super.onCreate();

        DB = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "db").fallbackToDestructiveMigration().build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL) //Базовая часть адреса
                .addConverterFactory(GsonConverterFactory.create()) //Конвертер, необходимый для преобразования JSON'а в объекты
                .build();

        availableLanguageApi = retrofit.create(AvailableLanguageApi.class); //Создаем объект, при помощи которого будем выполнять запросы

        translateApi = retrofit.create(TranslateApi.class);
    }

    public static AvailableLanguageApi getAvailableLanguageApi() {
        return availableLanguageApi;
    }

    public static TranslateApi getTranslateApi() {
        return translateApi;
    }

    public static AppDatabase DB() {
        return DB;
    }
}
