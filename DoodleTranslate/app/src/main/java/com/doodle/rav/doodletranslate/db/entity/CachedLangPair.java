package com.doodle.rav.doodletranslate.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import com.doodle.rav.doodletranslate.db.converter.MapConverter;

import java.util.Map;

/**
 * Created by rav on 21.05.2018.
 */

@Entity(tableName = "cached_lang_pair")
public class CachedLangPair {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "form")
    private String codeFromLang;

    @ColumnInfo(name = "to")
    @TypeConverters({MapConverter.class})
    private Map<String, String> codeToLangs;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodeFromLang() {
        return codeFromLang;
    }

    public void setCodeFromLang(String codeFromLang) {
        this.codeFromLang = codeFromLang;
    }

    public Map<String, String> getCodeToLangs() {
        return codeToLangs;
    }

    public void setCodeToLangs(Map<String, String> codeToLangs) {
        this.codeToLangs = codeToLangs;
    }
}
