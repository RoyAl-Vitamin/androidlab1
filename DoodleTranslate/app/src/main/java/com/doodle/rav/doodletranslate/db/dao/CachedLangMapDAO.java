package com.doodle.rav.doodletranslate.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.doodle.rav.doodletranslate.db.entity.CachedLangPair;

import java.util.List;

/**
 * Created by rav on 21.05.2018.
 */

@Dao
public interface CachedLangMapDAO {

    @Query("SELECT * FROM cached_lang_pair WHERE form = :itemCode LIMIT 1")
    CachedLangPair getByFromCode(String itemCode);

    @Query("SELECT * FROM cached_lang_pair")
    List<CachedLangPair> getAll();

    @Insert
    void insertAll(CachedLangPair... cachedLangPair);

    @Delete
    void delete(CachedLangPair cachedLangPair);

    @Insert
    void insert(CachedLangPair cachedLangPair);
}
