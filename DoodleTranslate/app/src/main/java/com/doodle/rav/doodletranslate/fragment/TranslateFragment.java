package com.doodle.rav.doodletranslate.fragment;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.doodle.rav.doodletranslate.R;
import com.doodle.rav.doodletranslate.application.App;
import com.doodle.rav.doodletranslate.db.entity.CachedTranslation;
import com.doodle.rav.doodletranslate.utils.SharedViewModel;
import com.doodle.rav.doodletranslate.utils.StorageFactory;
import com.doodle.rav.doodletranslate.web.response.TranslateResponse;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rav on 08.05.2018.
 */

@EFragment(R.layout.fragment_translate)
public class TranslateFragment extends Fragment {

    private static final String TAG = "TranslateFragment";

    private String key;

    private SharedViewModel model;

    @Bean
    StorageFactory storageFactory;

    @ViewById(R.id.sp_first_lang)
    Spinner spFirstLang;

    @ViewById(R.id.sp_second_lang)
    Spinner spSecondLang;

    @ViewById(R.id.input_text)
    EditText inputText;

    @ViewById(R.id.output_text)
    EditText outputText;

    String first = null;
    String second = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);
        model.getApiKey().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                Log.d(TAG, "New value token == " + s);
                key = s;
            }
        });
        model.getMapSecondLang().observe(this, new Observer<Map<String, String>>() {
            @Override
            public void onChanged(@Nullable Map<String, String> map) {
                List<String> list = new ArrayList<>();
                for (Map.Entry<String, String> entry : map.entrySet()) {
                    list.add(entry.getValue());
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, list);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                Log.d(TAG, "List has " + list.size() + " elements");
                spSecondLang.setAdapter(adapter);
                if (first != null && second != null) {
                    spSecondLang.setSelection(list.indexOf(first));
                    spFirstLang.setSelection(list.indexOf(second));
                    first = null;
                    second = null;
                }
            }
        });
    }

    @AfterViews
    void afterViews() {
        List<String> list = new ArrayList<>();
        for (Map.Entry<String, String> entry : mapAvailableLanguage.entrySet()) {
            list.add(entry.getValue());
        }

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spFirstLang.setAdapter(adapter);
        spFirstLang.setSelection(list.indexOf("русский"));
//        adapter.notifyDataSetChanged();

        spFirstLang.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                storageFactory.getMapAvailableLang(getCodeByValue(adapter.getItem(position)));
//                mapSecondLang.setValue(newMap);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    public static String getCodeByValue(String value) {
        for (Map.Entry<String, String> entry : mapAvailableLanguage.entrySet()) {
            if (value.equals(entry.getValue())) {
                return entry.getKey();
            }
        }
        throw new RuntimeException("Нужный язык не найден");
    }

    public static String getValueByCode(String code) {
        for (Map.Entry<String, String> entry : mapAvailableLanguage.entrySet()) {
            if (code.equals(entry.getKey())) {
                return entry.getValue();
            }
        }
        throw new RuntimeException("Нужный язык не найден");
    }


    @Click(R.id.swap)
    void swapClick() {

        first = (String) spFirstLang.getSelectedItem();
        second = (String) spSecondLang.getSelectedItem();

        int newFirstPosition = ((ArrayAdapter<String>) spFirstLang.getAdapter()).getPosition(second);
        int newSecondPosition = ((ArrayAdapter<String>) spSecondLang.getAdapter()).getPosition(first);

        spSecondLang.setSelection(newSecondPosition);
        spFirstLang.setSelection(newFirstPosition);
    }

    @Click(R.id.translate)
    void translate() {
        if (inputText.getText().toString().trim().length() == 0) return;

        searchTranslation();
    }

    @Background
    void searchTranslation() {
        final String text = inputText.getText().toString();

        Log.d(TAG, "from " + spFirstLang.getSelectedItem().toString());
        Log.d(TAG, "to " + spSecondLang.getSelectedItem().toString());

        final String lang = getCodeByValue(spFirstLang.getSelectedItem().toString()) + "-" + getCodeByValue(spSecondLang.getSelectedItem().toString());

        CachedTranslation cachedTranslationRead = App.DB().cachedTranslationDAO().findByTextAndLang(text, lang);
        if (cachedTranslationRead != null) {
            Log.d(TAG, "TEXT INPUT == " + cachedTranslationRead.getTextInput()
                    + "\nTEXT OUTPUT == " + cachedTranslationRead.getTextOutput()
                    + "\nLANG == " + cachedTranslationRead.getCodeFromToLang()
            );

            showTranslatedText(cachedTranslationRead.getTextOutput());
        } else {
            recordNotFound(text, lang);
        }
    }

    @UiThread
    void showTranslatedText(String text) {
        outputText.setText(text);
    }

    @UiThread
    void recordNotFound(final String text, final String lang) {
        Log.d(TAG, "Not Yet Cached");
        App.getTranslateApi().getData(
                key,
                text,
                lang,
                "plain",
                "0"
        ).enqueue(new Callback<TranslateResponse>() {
            @Override
            public void onResponse(Call<TranslateResponse> call, Response<TranslateResponse> response) {
                if (response.body() == null) {
                    Log.w(TAG, "response.body() == null");
                    return;
                } else if (response.body().getCode() != 200) {
                    Log.e(TAG, response.body().getCode() + ": " + response.body().getMessageByCode());
                    Toast.makeText(getActivity(), response.body().getCode() + ": " + response.body().getMessageByCode(), Toast.LENGTH_LONG);
                    return;
                }
                StringBuilder sb = new StringBuilder();
                for (String value : response.body().getText()) {
                    Log.d(TAG, "TEXT_TRANSLATE == " + value);
                    sb.append(value);
                }
                String translatedText = sb.toString();
                outputText.setText(translatedText);
                pasteInDB(lang, text, translatedText);
            }

            @Override
            public void onFailure(Call<TranslateResponse> call, Throwable t) {
                Log.e(TAG, "Ошибка при получении данных или возможно парсинге значений");
            }
        });
    }

    @Background
    void pasteInDB(String lang, String textInput, String textOutput) {

        CachedTranslation cachedTranslation = new CachedTranslation();
        cachedTranslation.setCodeFromToLang(lang);
        cachedTranslation.setTextInput(textInput);
        cachedTranslation.setTextOutput(textOutput);

        App.DB().cachedTranslationDAO().insert(cachedTranslation);
    }

    // Map поддерживаемых языков
    private static final Map<String, String> mapAvailableLanguage = new HashMap<String, String>() {{
        put("az", "азербайджанский");
        put("ml", "малаялам");
        put("sq", "албанский");
        put("mt", "мальтийский");
        put("am", "амхарский");
        put("mk", "македонский");
        put("en", "английский");
        put("mi", "маори");
        put("ar", "арабский");
        put("mr", "маратхи");
        put("hy", "армянский");
        put("mhr", "марийский");
        put("af", "африкаанс");
        put("mn", "монгольский");
        put("eu", "баскский");
        put("de", "немецкий");
        put("ba", "башкирский");
        put("ne", "непальский");
        put("be", "белорусский");
        put("no", "норвежский");
        put("bn", "бенгальский");
        put("pa", "панджаби");
        put("my", "бирманский");
        put("pap", "папьяменто");
        put("bg", "болгарский");
        put("fa", "персидский");
        put("bs", "боснийский");
        put("pl", "польский");
        put("cy", "валлийский");
        put("pt", "португальский");
        put("hu", "венгерский");
        put("ro", "румынский");
        put("vi", "вьетнамский");
        put("ru", "русский");
        put("ht", "гаитянский (креольский)");
        put("ceb", "себуанский");
        put("gl", "галисийский");
        put("sr", "сербский");
        put("nl", "голландский");
        put("si", "сингальский");
        put("mrj", "горномарийский");
        put("sk", "словацкий");
        put("el", "греческий");
        put("sl", "словенский");
        put("ka", "грузинский");
        put("sw", "суахили");
        put("gu", "гуджарати");
        put("su", "сунданский");
        put("da", "датский");
        put("tg", "таджикский");
        put("he", "иврит");
        put("th", "тайский");
        put("yi", "идиш");
        put("tl", "тагальский");
        put("id", "индонезийский");
        put("ta", "тамильский");
        put("ga", "ирландский");
        put("tt", "татарский");
        put("it", "итальянский");
        put("te", "телугу");
        put("is", "исландский");
        put("tr", "турецкий");
        put("es", "испанский");
        put("udm", "удмуртский");
        put("kk", "казахский");
        put("uz", "узбекский");
        put("kn", "каннада");
        put("uk", "украинский");
        put("ca", "каталанский");
        put("ur", "урду");
        put("ky", "киргизский");
        put("fi", "финский");
        put("zh", "китайский");
        put("fr", "французский");
        put("ko", "корейский");
        put("hi", "хинди");
        put("xh", "коса");
        put("hr", "хорватский");
        put("km", "кхмерский");
        put("cs", "чешский");
        put("lo", "лаосский");
        put("sv", "шведский");
        put("la", "латынь");
        put("gd", "шотландский");
        put("lv", "латышский");
        put("et", "эстонский");
        put("lt", "литовский");
        put("eo", "эсперанто");
        put("lb", "люксембургский");
        put("jv", "яванский");
        put("mg", "малагасийский");
        put("ja", "японский");
        put("ms", "малайский");
    }};
}
