package com.doodle.rav.doodletranslate.utils;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import java.util.Map;

/**
 * Created by rav on 10.05.2018.
 */

// ViewModel для хранения apiKey и передачи его между активити
public class SharedViewModel extends ViewModel {

    private final MutableLiveData<String> apiKey = new MutableLiveData<>();

    // список досупных к переводу языков
    private final MutableLiveData<Map<String, String>> mapSecondLang = new MutableLiveData<>();

    public void setApiKey(String item) {
        apiKey.setValue(item);
    }

    public LiveData<String> getApiKey() {
        return apiKey;
    }

    public void setMapSecondLang(Map<String, String> map) {
        mapSecondLang.setValue(map);
    }

    public void postMapSecondLang(Map<String, String> map) {
        mapSecondLang.postValue(map);
    }

    public LiveData<Map<String, String>> getMapSecondLang() {
        return mapSecondLang;
    }
}
