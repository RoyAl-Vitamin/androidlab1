package com.doodle.rav.doodletranslate.utils;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

import com.doodle.rav.doodletranslate.application.App;
import com.doodle.rav.doodletranslate.db.entity.CachedLangPair;
import com.doodle.rav.doodletranslate.web.response.LangResponse;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.doodle.rav.doodletranslate.fragment.TranslateFragment.getValueByCode;

/**
 * Created by rav on 21.05.2018.
 */

@EBean
public class StorageFactory {

    private static final String TAG = "StorageFactory";

    private SharedViewModel model;

    @RootContext
    Activity activity;

    @AfterInject
    public void ititStorageFactory() {

        model = ViewModelProviders.of((FragmentActivity) activity).get(SharedViewModel.class);

//        getMapAvailableLang("en");
        getMapAvailableLang("ru");
    }

    @Background
    public void getMapAvailableLang(final String itemCode) {
        Log.d(TAG, "Selected item code == " + itemCode);

        CachedLangPair pair = App.DB().cachedLangMapDAO().getByFromCode(itemCode);
        if (pair == null) {
            Log.d(TAG, "COULDN'T FIND PAIR with langCode == " + itemCode);
            App.getAvailableLanguageApi().getData(
                    model.getApiKey().getValue(),
                    itemCode
            ).enqueue(new Callback<LangResponse>() {
                @Override
                public void onResponse(Call<LangResponse> call, Response<LangResponse> response) {
                    if (response.body() == null) {
                        Log.e(TAG, "response.body() == null");
                        model.setMapSecondLang(Collections.<String, String>emptyMap());
                        return;
                    } else if (response.body().getCode() != null) {
                        Toast.makeText(activity, response.body().getCode() + ": " + response.body().getMessageByCode(), Toast.LENGTH_LONG);
                        Log.e(TAG, "error with code: " + response.body().getCode() + " with message: " + response.body().getMessageByCode());
                        model.setMapSecondLang(Collections.<String, String>emptyMap());
                        return;
                    }

                    // На всякий случай приводим к нижнему регистру
                    Map<String, String> mapAvailableLang = new HashMap<>();
                    for (Map.Entry<String, String> entry : response.body().getLangs().entrySet()) {
//                        Log.d(TAG, entry.getKey().toLowerCase() + ":" + entry.getValue().toLowerCase());
                        mapAvailableLang.put(entry.getKey().toLowerCase(), getValueByCode(entry.getKey().toLowerCase())); // entry.getValue().toLowerCase()
                    }

                    // Сохраняем в БД
                    CachedLangPair cachedLangPair = new CachedLangPair();
                    cachedLangPair.setCodeFromLang(itemCode);
                    cachedLangPair.setCodeToLangs(mapAvailableLang);
                    pasteIntoDB(cachedLangPair);

                    model.setMapSecondLang(mapAvailableLang);

                }

                @Override
                public void onFailure(Call<LangResponse> call, Throwable t) {
                    Log.e(TAG, "Ошибка при получении данных или возможно парсинге значений");
                    model.setMapSecondLang(Collections.<String, String>emptyMap());
                }
            });
        } else {
            Log.d(TAG, "FIND PAIR with langCode == " + itemCode);
            model.postMapSecondLang(pair.getCodeToLangs());
        }
    }

    @Background
    void pasteIntoDB(CachedLangPair cachedLangPair) {
        Log.d(TAG, "Paste: " + cachedLangPair.getCodeFromLang() + " to lang size == " + cachedLangPair.getCodeToLangs().size());
        App.DB().cachedLangMapDAO().insert(cachedLangPair);
    }
}
